/**
 * \author Nate Shimp shimpjn@protonmail.com
 *
 * Netgraph Eve output file plugin.
 *
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netgraph.h>
#include <assert.h>

#include "conf.h"

#include "netgraph-eve-plugin.h"

struct NgCtx {
    int ds;
    const char *hook;
};

struct NgConfig {
    const char *name;
    const char *hook;
    const char *peer;
    const char *peerhook;
};

bool NgGetConfig(ConfNode *conf, struct NgConfig *config)
{
    (void)ConfGetChildValue(conf, "name", &config->name);

    int rc = ConfGetChildValue(conf, "hook", &config->hook);
    if (rc == 0) return false;

    ConfNode *peerConf = ConfGetChildWithDefault(conf, NULL, "peer");
    if (peerConf == NULL) return false;

    rc = ConfGetChildValue(peerConf, "name", &config->peer);
    if (rc == 0) return false;

    rc = ConfGetChildValue(peerConf, "hook", &config->peerhook);
    if (rc == 0) return false;

    return true;
}

int NgOpen(ConfNode *conf, void **data)
{
    struct NgConfig config;
    bool configured = NgGetConfig(conf, &config);
    if (!configured) return -1;

    int rc, cs, ds;
    rc = NgMkSockNode(config.name, &cs, &ds);
    if (rc == -1) return -1;

    struct ngm_connect message;
    rc = snprintf(message.path, NG_PATHSIZ, "%s:", config.peer);
    if (rc < 0) goto error;

    (void)strncpy(message.ourhook, config.hook, NG_HOOKSIZ);
    (void)strncpy(message.peerhook, config.peerhook, NG_HOOKSIZ);

    /* ".:" represents the "relative path" so if you send a message to node X,
     * it will interpret a path of ".:" as itself.
     */
    rc = NgSendMsg(cs, ".:", NGM_GENERIC_COOKIE, NGM_CONNECT, &message, sizeof(message));
    if (rc == -1) goto error;

    struct NgCtx *ctx = (struct NgCtx *)calloc(1, sizeof(struct NgCtx));
    if (ctx == NULL) goto error;

    ctx->ds = ds;
    ctx->hook = config.hook; 

    *data = (void *)ctx;

    close(cs);
    return 0;
error:
    close(cs);
    close(ds);
    return -1;
}

int NgWrite(const char *buffer, int buffer_len, void *ctx)
{
    struct NgCtx *data = (struct NgCtx *)ctx;
    return NgSendData(data->ds, data->hook, (const u_char *)buffer, buffer_len);
}

void NgClose(void *ctx)
{
    struct NgCtx *data = (struct NgCtx *)ctx;

    close(data->ds);
    free(data);

    data = NULL;
}

void NgInit(void)
{
    SCPluginFileType *plugin = (SCPluginFileType *)calloc(1, sizeof(SCPluginFileType));
    if (plugin == NULL) return;

    plugin->name = PLUGIN_NAME;
    plugin->Open = NgOpen;
    plugin->Write = NgWrite;
    plugin->Close = NgClose;

    bool registered = SCPluginRegisterFileType(plugin);
    if (!registered) free(plugin);
}

SCPlugin *SCPluginRegister()
{
    SCPlugin *plugin = (SCPlugin *)calloc(1, sizeof(SCPlugin));
    if (plugin == NULL) return NULL;

    plugin->name = PLUGIN_NAME;
    plugin->license = "MIT";
    plugin->author = "Nate Shimp <shimpjn@protonmail.com>";
    plugin->Init = NgInit;

    return plugin;
}
