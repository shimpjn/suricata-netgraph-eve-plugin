CFLAGS+=-Werror -Wall -pedantic -std=c17 -I${SURICATA_SRC} -fPIC
LDFLAGS+=-lc -lnetgraph

netgraph-eve-plugin.so: netgraph-eve-plugin.o
	${CC} -shared -o $@ $> ${LDFLAGS}

netgraph-eve-plugin.o: netgraph-eve-plugin.c netgraph-eve-plugin.h

.PHONY: clean
clean:
	rm -f *.so *.o
