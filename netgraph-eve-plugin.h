/**
 * \author Nate Shimp shimpjn@protonmail.com
 *
 * Netgraph Eve output file plugin.
 *
 */

#ifndef __NETGRAPH_EVE_PLUGIN_H__
#define __NETGRAPH_EVE_PLUGIN_H__
#define PLUGIN_NAME "netgraph";

#include "suricata-plugin.h"

SCPlugin *SCPluginRegister(void);

#endif // __NETGRAPH_EVE_PLUGIN_H__
